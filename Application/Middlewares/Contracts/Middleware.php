<?php


namespace Application\Middlewares\Contracts;


abstract class Middleware
{
    public function __construct()
    {
        $this->handle();
    }
    abstract protected function handle();
}