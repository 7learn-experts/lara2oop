<?php
return [
    '/' => 'HomeController@index',
    '/admin/users'  => [
        'target' => 'Admin\\UsersController@index',
        'middleware'  => 'auth'
    ]
];