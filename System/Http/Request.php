<?php


namespace System\Http;


class Request
{
    private $data;

    public function __construct()
    {
        $this->data = $_REQUEST;
    }

    public function all()
    {
        return $this->data;
    }

    public function input($key)
    {
        if(in_array($key,array_keys($this->data)))
        {
            return $this->data[$key];
        }
        return null;
    }
}